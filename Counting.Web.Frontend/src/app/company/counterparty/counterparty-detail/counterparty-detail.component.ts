import { Component, OnInit } from '@angular/core';
import { Counterparty } from "app/_models/catalog/Counterparty";
import { CounterpartyService } from "app/company/counterparty/counterparty.service";
import { Router, ActivatedRoute } from "@angular/router";
import { NotificationService } from "app/_services/notification.service";
import { NgForm } from "@angular/forms";

@Component({
  selector: 'app-counterparty-detail',
  templateUrl: './counterparty-detail.component.html',
  styleUrls: ['./counterparty-detail.component.css']
})
export class CounterpartyDetailComponent implements OnInit {

  currentCounterparty: Counterparty;
  isEdit: boolean;

  constructor(private routeSnapshot: ActivatedRoute,
    private counterpartyService: CounterpartyService,
    private router: Router,
    private notificationService: NotificationService) { }

  ngOnInit() {
    this.currentCounterparty = new Counterparty();
    var id = this.routeSnapshot.snapshot.params['id'];

    if (id) {
      this.isEdit = true;
      this.counterpartyService.getCounterparty(id)
        .then(counterparty => {
          this.currentCounterparty = counterparty;
        })
        .catch(() => {
          this.notificationService.addUnableToLoad();
          this.router.navigate(["counterparty"]);
        });
    }
  }


  onSaveOrCreate(form: NgForm) {

    if (!form.valid) {
      Object.keys(form.controls).forEach(key => {

        form.control.get(key).markAsTouched();
      });
      return;
    }

    this.currentCounterparty.email = form.control.get("Email").value;
    this.currentCounterparty.name = form.control.get("Name").value;
    this.currentCounterparty.phone = form.control.get("Phone").value;
    this.currentCounterparty.sureName = form.control.get("Surename").value;
    
    if (this.currentCounterparty.id == null)
      this.createCounterparty();
    else
      this.updateCounterparty();

  }

  createCounterparty() {
    this.counterpartyService.addCounterparty(this.currentCounterparty)
      .then(() => {
        this.notificationService.addSuccessUpdate();
        this.router.navigate(["/counterparty"]);
      })
      .catch(() => {
        this.notificationService.addFailedToUpdate();
      });
  }

  updateCounterparty() {
    this.counterpartyService.updateCounterparty(this.currentCounterparty)
      .then(() => {
        this.notificationService.addEditUpdate();
        this.router.navigate(["/counterparty"]);
      })
      .catch(() => {
        this.notificationService.addFailedToUpdate();
      });
  }



}
