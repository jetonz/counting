import { Observable } from 'rxjs/Observable';
import { Injectable } from "@angular/core";

import { Counterparty } from "app/_models/catalog/Counterparty";

@Injectable()
export class CounterpartyService {

    constructor() {

    }


    private counterpartyList: Counterparty[] = [
        new Counterparty("1", "Васька", "Волков", "+38097-55-546-79", "mail@tr.ri", 256),
        new Counterparty("2", "Вовка", null, "+38097-20-176-21", "sdshd@fh.ert", -562),
        new Counterparty("3", "Юрка", "Шаламай", "+38097-45-568-93", null, 0),
        new Counterparty("4", "Петька", "Морозов", null, null, 32.26),
        new Counterparty("5", "Игорёшка", "Запугайло", "+38097-08-246-61", null, -5000),
    ];

    public getCounterparties(): Promise<Counterparty[]> {
        return new Promise((resolve, reject) => {
            try {
                resolve(this.counterpartyList.slice());
            }
            catch (ex) {
                reject();
            }
        });
    }


    public getCounterparty(id: string): Promise<Counterparty> {
        return new Promise((resolve, reject) => {

            this.counterpartyList.forEach(element => {
                if (element.id === id)
                    resolve(element);
            });

            reject();
        });
    }

    public addCounterparty(newCounterparty: Counterparty): Promise<boolean> {
        return new Promise((resolve, reject) => {

            try {
                newCounterparty.id = '' + (this.counterpartyList.length + 1);
                this.counterpartyList.push(newCounterparty);
                resolve(true);
            }
            catch (Exc) {
                reject(false);
            }
        }

        );
    }

    public updateCounterparty(counterparty: Counterparty): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let success = false;
            this.counterpartyList.forEach((element, index) => {

                if (element.id == counterparty.id) {
                    element.name = counterparty.name;
                    element.phone = counterparty.phone;
                    element.sureName = counterparty.sureName;
                    success = true;
                    return;

                }
            });

            (success) ? resolve(true) : reject(false);

        });
    }

    public deleteCounterparty(id: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let success = false;
            this.counterpartyList.forEach((element, index) => {

                if (element.id == id) {
                    this.counterpartyList.splice(index, 1);
                    success = true;
                    return;

                }
            });

            (success) ? resolve(true) : reject(false);

        });
    }

    public filterCounterparty(text: string): Promise<Counterparty[]> {
        return new Promise((resolve, reject) => {
            if(text == ''){
                resolve(this.counterpartyList.slice());
                return;
            }
                
            let filteredList: Counterparty[] = [];
            this.counterpartyList.forEach((element, index) => {
 
                if (element.name.startsWith(text)) {
                    filteredList.push(element);
                }

                if (index+1 == this.counterpartyList.length) {
                   
                    resolve(filteredList);
                }

            });
        });
    }
}