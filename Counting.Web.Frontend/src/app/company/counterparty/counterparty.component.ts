import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from "@angular/router";

import { NotificationService } from "app/_services/notification.service";
import { Counterparty } from "app/_models/catalog/Counterparty";
import { CounterpartyListComponent } from "app/company/counterparty/counterparty-list/counterparty-list.component";

@Component({
  selector: 'app-counterparty',
  templateUrl: './counterparty.component.html',
  styleUrls: ['./counterparty.component.css']
})
export class CounterpartyComponent implements OnInit {

  selectedCounterparty: Counterparty;
  searchWord: string = '';

  private textFilterTimeoutFn: any;
  private textFilterExecuting: boolean;

  @ViewChild('listComponent') child : CounterpartyListComponent;

  constructor(private router: Router, private notificationServ: NotificationService) { }

  ngOnInit() {
  }

  clearSearch() {
    if (this.searchWord != '') {
      this.searchWord = '';
      this.onFilterTextEdit();
      //this.child.tryLoaCounterparties();
    }

  }

  onFilterTextEdit() {

    if (this.textFilterExecuting) {
      clearTimeout(this.textFilterTimeoutFn);
    }

    this.textFilterExecuting = true;
    this.textFilterTimeoutFn = setTimeout(() => {
      this.child.filterCounterparty(this.searchWord);
      this.textFilterExecuting = false;
    }, 700);
  }


}

