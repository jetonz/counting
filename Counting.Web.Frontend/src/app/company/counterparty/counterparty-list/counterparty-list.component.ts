import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Counterparty } from "app/_models/catalog/Counterparty";
import { CounterpartyService } from "app/company/counterparty/counterparty.service";
import { NotificationService } from "app/_services/notification.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-counterparty-list',
  templateUrl: './counterparty-list.component.html',
  styleUrls: ['./counterparty-list.component.css']
})
export class CounterpartyListComponent implements OnInit {

  selectedCounterpartyItem: Counterparty;
  counterpartyList: Counterparty[] = [];

  constructor(
    private counterpartyService: CounterpartyService,
    private notifcationServ: NotificationService,
    private router: Router)
  { }

  @Output() selectedCounterparty = new EventEmitter<Counterparty>();

  ngOnInit() {
    this.tryLoaCounterparties();
  }

  ngOnDestroy(): void {

  }

  onItemSelected(choosedCounterparty: Counterparty) {
    this.selectedCounterpartyItem = choosedCounterparty;
    this.selectedCounterparty.emit(this.selectedCounterpartyItem);
  }

  tryLoaCounterparties() {
    this.counterpartyService.getCounterparties()
      .then(
      data => {
        this.onItemSelected(null);
        this.counterpartyList = data;
      })
      .catch(() =>
        this.notifcationServ.addUnableToLoad()
      );
  }

  goToDetail(cashbox: Counterparty) {
    this.router.navigate(["counterparty", "edit", cashbox.id]);
  }

  deleteCounterparty() {
    this.counterpartyService.deleteCounterparty(this.selectedCounterpartyItem.id)
      .then(() => {
        this.notifcationServ.addDeleteUpdate();
        this.tryLoaCounterparties();
      })
      .catch(() => { this.notifcationServ.addFailedToUpdate() })
  }

  filterCounterparty(text: string){
    this.counterpartyService.filterCounterparty(text)
    .then((data)=>{
      this.counterpartyList = data;
    });
  }

}
