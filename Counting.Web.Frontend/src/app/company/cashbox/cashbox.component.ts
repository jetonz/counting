import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from "@angular/router";

import { Cashbox } from "app/_models/catalog/Cashbox";
import { NotificationService } from "app/_services/notification.service";
import { CashboxListComponent } from "app/company/cashbox/cashbox-list/cashbox-list.component";

@Component({
  selector: 'app-cashbox',
  templateUrl: './cashbox.component.html',
  styleUrls: ['./cashbox.component.css']
})
export class CashboxComponent implements OnInit {

  selectedCashbox: Cashbox;
  
  @ViewChild('listComponent') child : CashboxListComponent;

  constructor(
    private router: Router,
    private notificationServ: NotificationService) { }

  ngOnInit() {
  }

  
}
