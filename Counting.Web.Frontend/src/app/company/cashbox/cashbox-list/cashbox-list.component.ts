import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Router } from "@angular/router";

import { Cashbox } from "app/_models/catalog/Cashbox";
import { CashboxService } from "app/company/cashbox/cashbox.service";
import { NotificationService } from "app/_services/notification.service";

@Component({
  selector: 'app-cashbox-list',
  templateUrl: './cashbox-list.component.html',
  styleUrls: ['./cashbox-list.component.css']
})
export class CashboxListComponent implements OnInit {

  @Output() selectedCashbox = new EventEmitter<Cashbox>();
  
  selectedCashboxItem: Cashbox;
  cashboxList: Cashbox[] = [];

  constructor(
    private cashboxService: CashboxService, 
    private notifcationServ: NotificationService,
    private router: Router) 
    { }



  ngOnInit() {   
    this.tryLoadCashboxes();
  }

  ngOnDestroy(): void {
   
  }

  onItemSelected(choosedWarehoues: Cashbox){
    this.selectedCashboxItem = choosedWarehoues;
    this.selectedCashbox.emit(this.selectedCashboxItem);
  }

  tryLoadCashboxes() {
    this.cashboxService.getCashboxes()
      .then(
      data => {
        this.cashboxList = data;
      })
      .catch(() =>
        this.notifcationServ.addUnableToLoad()
      );
  }

  goToDetail(cashbox: Cashbox){
    this.router.navigate(["cashbox", "edit", cashbox.id]);
  }

  deleteCashbox(){
    this.cashboxService.deleteCashbox(this.selectedCashboxItem.id)
          .then(()=>{ 
            this.notifcationServ.addDeleteUpdate(); 
            this.onItemSelected(null);
            this.tryLoadCashboxes();
          })
          .catch(()=>{ this.notifcationServ.addFailedToUpdate() })
  }
  
}
