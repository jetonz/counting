import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { NgForm } from "@angular/forms";

import { Cashbox } from "app/_models/catalog/Cashbox";
import { CashboxService } from "app/company/cashbox/cashbox.service";
import { NotificationService } from "app/_services/notification.service";

@Component({
  selector: 'app-cashbox-detail',
  templateUrl: './cashbox-detail.component.html',
  styleUrls: ['./cashbox-detail.component.css']
})
export class CashboxDetailComponent implements OnInit {

  currentCashbox: Cashbox;
  isEdit: boolean;

  constructor(private routeSnapshot: ActivatedRoute,
    private cashboxService: CashboxService,
    private router: Router,
    private notificationService: NotificationService) {

  }

  ngOnInit() {

    this.currentCashbox = new Cashbox();
    var id = this.routeSnapshot.snapshot.params['id'];

    if (id) {
      this.isEdit = true;
      this.cashboxService.getCashbox(id)
        .then(cashbox => {
          this.currentCashbox = cashbox;
        })
        .catch(() => {
          this.notificationService.addUnableToLoad();
          this.router.navigate(["bashbox"]);
        });
    }

  }

  onSaveOrCreate(form: NgForm) {

    if (!form.valid) {
      Object.keys(form.controls).forEach(key => {
        
        form.control.get(key).markAsTouched();
      });
      return;
    }

    if (!this.currentCashbox.id) {
      this.createWarehouse(form);
    }
    else {
      this.updateWarehouse(form);
    }
  }

  private createWarehouse(form: NgForm) {

    let cashbox = new Cashbox();
    cashbox.name = form.value["cashboxName"];
    this.cashboxService
      .addCashbox(cashbox)
      .then(
      () => {
        this.notificationService.addSuccessUpdate();
        this.router.navigate(["cashbox"]);
      }
      )
      .catch(
      () => {
        this.notificationService.addFailedToUpdate();
      }
      );
  }

  private updateWarehouse(form: NgForm) {

    this.currentCashbox.name = form.value["cashboxName"];
    this.cashboxService
      .updateCashbox(this.currentCashbox)
      .then(
      () => {
        this.notificationService.addEditUpdate();
        this.router.navigate(["cashbox"]);
      }
      )
      .catch(
      () => {
        this.notificationService.addFailedToUpdate();
      }
      );
  }

}
