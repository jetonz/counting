import { Cashbox } from "../../_models/catalog/Cashbox";
import { Observable } from 'rxjs/Observable';
import { Injectable } from "@angular/core";

@Injectable()
export class CashboxService {

    constructor() {

    }


    private cashboxList: Cashbox[] = [
        new Cashbox("1", "Касса 1", 100),
        new Cashbox("2", "Касса 2", 0),
        new Cashbox("3", "Касса 3", 2500),
        new Cashbox("4", "Касса 4", -256.23),
        new Cashbox("5", "Касса 5", 6895.14),
    ];

    public getCashboxes(): Promise<Cashbox[]> {
        return new Promise((resolve,reject) => {
            try{
               resolve(this.cashboxList.slice());
            }
            catch(ex){
                reject();
            }            
        });
    }


    public getCashbox(id: string): Promise<Cashbox> {
        return new Promise((resolve, reject) => {
                         
              this.cashboxList.forEach(element => {
                    if (element.id === id)
                        resolve(element);
                });

                reject();
        });
    }

    public addCashbox(newCashbox: Cashbox): Promise<boolean> {
        return new Promise((resolve, reject) => {
           
                try {
                    newCashbox.id = '' + (this.cashboxList.length+1);
                    newCashbox.deposit = 0;
                    this.cashboxList.push(newCashbox);
                    resolve(true);
                }
                catch (Exc) {
                    reject(false);
                }
            }
           
        );
    }

    public updateCashbox(cashbox: Cashbox): Promise<boolean> {
        return new Promise((resolve, reject) => {
                let success = false;
                this.cashboxList.forEach((element, index) => {

                    if (element.id == cashbox.id) {
                        element.name = cashbox.name;
                        success = true;
                        return;
                        
                    }                   
                });

                (success) ? resolve(true) : reject(false);
          
        });
    }

    public deleteCashbox(id: string){
         return new Promise((resolve, reject) => {
                let success = false;
                this.cashboxList.forEach((element, index) => {

                    if (element.id == id) {
                        this.cashboxList.splice(index,1);
                        success = true;
                        return;                        
                    }                   
                });

                (success) ? resolve(true) : reject(false);
          
        });
    }


}