import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from "@angular/router";
import { Warehouse } from "../../_models/catalog/Warehouse";
import { NotificationService } from "app/_services/notification.service";
import { WarehouseListComponent } from "app/company/warehouse/warehouse-list/warehouse-list.component";


@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.css'],
})
export class WarehouseComponent implements OnInit {

  selectedWarehouse: Warehouse;
  @ViewChild('listComponent') child: WarehouseListComponent;

  constructor(private router: Router, private notificationServ: NotificationService) { }

  ngOnInit() {
  }




}
