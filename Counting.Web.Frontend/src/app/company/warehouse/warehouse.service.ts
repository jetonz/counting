import { Warehouse } from "../../_models/catalog/Warehouse";
import { Observable } from 'rxjs/Observable';
import { Injectable } from "@angular/core";

@Injectable()
export class WarehouseService {

    constructor() {

    }


    private warehouseList: Warehouse[] = [
        new Warehouse("1", "Склад 1"),
        new Warehouse("2", "Склад 2"),
        new Warehouse("3", "Склад 3"),
        new Warehouse("4", "Склад 4"),
        new Warehouse("5", "Склад 5"),
    ];

    public getWarehouses(): Promise<Warehouse[]> {
        return new Promise((resolve,reject) => {
            try{
               resolve(this.warehouseList.slice());
            }
            catch(ex){
                reject();
            }            
        });
    }


    public getWarehouse(id: string): Promise<Warehouse> {
        return new Promise((resolve, reject) => {
                         
              this.warehouseList.forEach(element => {
                    if (element.id === id)
                        resolve(element);
                });

                reject();
        });
    }

    public addWarehouse(newWarehouse: Warehouse): Promise<boolean> {
        return new Promise((resolve, reject) => {
           
                try {
                    newWarehouse.id = '' + (this.warehouseList.length+1);
                    this.warehouseList.push(newWarehouse);
                    resolve(true);
                }
                catch (Exc) {
                    reject(false);
                }
            }
           
        );
    }

    public updateWarehouse(warehouse: Warehouse): Promise<boolean> {
        return new Promise((resolve, reject) => {
                let success = false;
                this.warehouseList.forEach((element, index) => {

                    if (element.id == warehouse.id) {
                        element.name = warehouse.name;
                        success = true;
                        return;
                        
                    }                   
                });

                (success) ? resolve(true) : reject(false);
          
        });
    }

      public deleteWarehouse(id: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
                let success = false;
                this.warehouseList.forEach((element, index) => {

                    if (element.id == id) {
                        this.warehouseList.splice(index,1);
                        success = true;
                        return;                        
                    }                   
                });

                (success) ? resolve(true) : reject(false);          
        });
    }
}