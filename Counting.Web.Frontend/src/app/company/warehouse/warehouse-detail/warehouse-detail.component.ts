import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Warehouse } from "app/_models/catalog/Warehouse";
import { WarehouseService } from "app/company/warehouse/warehouse.service";
import { NgForm } from "@angular/forms";
import { NotificationService } from "app/_services/notification.service";

@Component({
  selector: 'app-warehouse-detail',
  templateUrl: './warehouse-detail.component.html',
  styleUrls: ['./warehouse-detail.component.css'],

})
export class WarehouseDetailComponent implements OnInit {

  currentWarehouse: Warehouse;
  isEdit: boolean;

  constructor(private routeSnapshot: ActivatedRoute,
    private warehouseService: WarehouseService,
    private router: Router,
    private notificationService: NotificationService) {

  }

  ngOnInit() {

    this.currentWarehouse = new Warehouse();
    var id = this.routeSnapshot.snapshot.params['id'];

    if (id) {
      this.isEdit = true;
      this.warehouseService.getWarehouse(id)
        .then(warehouse => {
          this.currentWarehouse = warehouse;
        })
        .catch(() => {
          this.notificationService.addUnableToLoad();
          this.router.navigate(["warehous"]);
        });
    }

  }

  onSaveOrCreate(form: NgForm) {

    if (!form.valid) {
      Object.keys(form.controls).forEach(key => {
        
        form.control.get(key).markAsTouched();
      });
      return;
    }

    if (!this.currentWarehouse.id) {
      this.createWarehouse(form);
    }
    else {
      this.updateWarehouse(form);
    }
  }

  private createWarehouse(form: NgForm) {

    let warehouse = new Warehouse();
    warehouse.name = form.value["warehouseName"];
    this.warehouseService
      .addWarehouse(warehouse)
      .then(
      () => {
        this.notificationService.addSuccessUpdate();
        this.router.navigate(["warehous"]);
      }
      )
      .catch(
      () => {
        this.notificationService.addFailedToUpdate();
      }
      );
  }

  private updateWarehouse(form: NgForm) {

    this.currentWarehouse.name = form.value["warehouseName"];
    this.warehouseService
      .updateWarehouse(this.currentWarehouse)
      .then(
      () => {
        this.notificationService.addEditUpdate();
        this.router.navigate(["warehous"]);
      }
      )
      .catch(
      () => {
        this.notificationService.addFailedToUpdate();
      }
      );
  }



}
