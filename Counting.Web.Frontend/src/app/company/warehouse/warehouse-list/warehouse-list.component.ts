import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Router } from "@angular/router";

import { Warehouse } from "app/_models/catalog/Warehouse";
import { WarehouseService } from "app/company/warehouse/warehouse.service";
import { NotificationService } from "app/_services/notification.service";

@Component({
  selector: 'app-warehouse-list',
  templateUrl: './warehouse-list.component.html',
  styleUrls: ['./warehouse-list.component.css'], 
})
export class WarehouseListComponent implements OnInit, OnDestroy {

  @Output() selectedWarehouse = new EventEmitter<Warehouse>();

  selectedWarehouseItem: Warehouse;
  warehouseList: Warehouse[] = [];

  constructor(
    private warehouseService: WarehouseService, 
    private notifcationServ: NotificationService,
    private router: Router) 
    { }



  ngOnInit() {   
    this.tryLoadWarehouses();
  }

  ngOnDestroy(): void {
   
  }

  onItemSelected(choosedWarehoues: Warehouse){
    this.selectedWarehouseItem = choosedWarehoues;
    this.selectedWarehouse.emit(this.selectedWarehouseItem);
  }

  tryLoadWarehouses() {
    this.warehouseService.getWarehouses()
      .then(
      data => {
        this.warehouseList = data;
      })
      .catch(() =>
        this.notifcationServ.addUnableToLoad()
      );
  }

  goToDetail(warehouse: Warehouse){
    this.router.navigate(["warehous", "edit", warehouse.id]);
  }

   deleteWarehouse(){
    this.warehouseService.deleteWarehouse(this.selectedWarehouseItem.id)
          .then(()=>{ 
            this.notifcationServ.addDeleteUpdate(); 
            this.onItemSelected(null);
            this.tryLoadWarehouses();
          })
          .catch(()=>{ this.notifcationServ.addFailedToUpdate() })
  }

}
