import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule, ActivatedRouteSnapshot } from '@angular/router';

import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ProductComponent } from './product/product.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductDetailComponent } from './product/product-detail/product-detail.component';
import { WarehouseComponent } from './company/warehouse/warehouse.component';
import { CashboxComponent } from './company/cashbox/cashbox.component';
import { NotificationComponent } from './notification/notification.component';
import { CashboxListComponent } from './company/cashbox/cashbox-list/cashbox-list.component';
import { CashboxDetailComponent } from './company/cashbox/cashbox-detail/cashbox-detail.component';
import { WarehouseListComponent } from './company/warehouse/warehouse-list/warehouse-list.component';
import { WarehouseDetailComponent } from './company/warehouse/warehouse-detail/warehouse-detail.component';
import { CounterpartyComponent } from './company/counterparty/counterparty.component';
import { CounterpartyListComponent } from './company/counterparty/counterparty-list/counterparty-list.component';
import { CounterpartyDetailComponent } from './company/counterparty/counterparty-detail/counterparty-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import {SidebarDropdownDirective} from './sidebar/directives/sidebar-dropdown.directive';
import { TabsDirective } from "app/_directives/tabs.directive";
import { SidebarActiveDirective } from "app/sidebar/directives/sidebar-active.directive";
import { BalanceColorDirective } from './_directives/balance-color.directive';

import { appRoutes } from "app/app.route";

import { NotificationService } from "app/_services/notification.service";
import { WarehouseService } from "app/company/warehouse/warehouse.service";
import { CashboxService } from "app/company/cashbox/cashbox.service";
import { ProductService } from "app/product/product.service";
import { CounterpartyService } from "app/company/counterparty/counterparty.service";
import { BarcodeService } from "app/product/barcode.service";

import { BalanceColorPipe } from './_pipes/balance-color.pipe';



@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    ProductComponent,
    ProductListComponent,
    ProductDetailComponent,
    WarehouseComponent,
    WarehouseListComponent,
    WarehouseDetailComponent,
    CounterpartyComponent,
    CounterpartyListComponent,
    CounterpartyDetailComponent,
    DashboardComponent,    
    SidebarDropdownDirective,
    TabsDirective,
    SidebarActiveDirective,
    NotificationComponent,
    CashboxComponent,
    CashboxListComponent,
    CashboxDetailComponent,
    BalanceColorDirective,
    BalanceColorPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    
  ],
  providers: [
    NotificationService,
    CashboxService,
    CounterpartyService,
    BarcodeService,
    ProductService,
    WarehouseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
