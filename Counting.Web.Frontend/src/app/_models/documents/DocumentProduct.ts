export class DocumentProduct {

    constructor(
        public productId: string, 
        public productName: string,
        public warehouseId: string,
        public quantity: number = 0,
        public price: number = 0,

        ) {
        
    }
}