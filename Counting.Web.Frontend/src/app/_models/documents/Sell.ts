import { BaseDocument } from "../documents/BaseDocument";
import { Counterparty } from "../catalog/Counterparty";
import { DocumentProduct } from "../documents/DocumentProduct";

export class Sell extends BaseDocument {
        public createAt: Date;
        public total: number;
        public isSold: boolean;
        public comment: string;

        public customer: Counterparty;
        public documentBase: BaseDocument;
        public childDocument: BaseDocument[];
        public products: DocumentProduct[];
}