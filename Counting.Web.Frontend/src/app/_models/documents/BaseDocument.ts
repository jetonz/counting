import {DocumentType} from './enums/DocumentType';

export abstract class  BaseDocument {
    constructor(public id:string, public name: string, public documentType: DocumentType) {
        
    }
}