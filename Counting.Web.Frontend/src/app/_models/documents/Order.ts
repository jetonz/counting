
import { BaseDocument } from "../documents/BaseDocument";
import { Counterparty } from "../catalog/Counterparty";
import { OrderStatus } from "../documents/enums/OrderStatus";
import { DocumentProduct } from "../documents/DocumentProduct";

export class Order extends BaseDocument {
    public orderNumber: string;
    public total: number;
    public comment: string;
    public customer: Counterparty;
    public createAt: Date;    
    public status: OrderStatus;
    public childDocument: BaseDocument[];
    public products: DocumentProduct[];


}