import { BaseDocument } from "../documents/BaseDocument";
import { Counterparty } from "../catalog/Counterparty";
import { Cashbox } from "../catalog/CashBox";

export class Payment extends BaseDocument {
    public customer: Counterparty;
    public value: number;
    public baseDocument: BaseDocument;
    public comment: string;
    public createAt: Date;
    public cashBox: Cashbox;

}