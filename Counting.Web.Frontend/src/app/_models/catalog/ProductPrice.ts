import { PriceType } from "../catalog/PriceType";

export class ProductPrice {

    constructor(
        public id:string,
        public type: PriceType,
        public price: number,
    ){

    }
    
}