export class Counterparty {

    constructor(
        public id?: string,
        public name?: string,
        public sureName?: string,
        public phone?: string,
        public email?: string,
        private _deposit?: number
    ) {

        
    }

    public get deposit(): number{
        return this._deposit;
    };
}