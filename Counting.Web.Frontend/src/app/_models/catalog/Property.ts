import { PropertyType } from "../catalog/enums/PropertyType";

export class Property {
    public id: string;    
    public name: string;
    public type: PropertyType;
    public value: PropertyValue;
}

export class PropertyValue{
    public id: string;
    public value: string;
    public propertyId: string;
    public productId: string;
}