export class Barcode {
    constructor(
        public id: string,        
        public value: number,
        public productId: string
    ){

    }
}