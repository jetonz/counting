export class File{
    public id: string;
    public content: string;
    public price: number;
}