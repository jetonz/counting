import { Warehouse } from "../catalog/Warehouse";

export class ProductStock {

    constructor(
        public id: string,
        public warehouse: Warehouse,
        public quantity: number) {

    }
}