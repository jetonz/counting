export class Cashbox {

    constructor(
        public id?: string,
        public name?: string,
        public deposit?: number) {

    }
}