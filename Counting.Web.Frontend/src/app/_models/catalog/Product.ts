import { File } from './File';
import { ProductPrice } from "../catalog/ProductPrice";
import { ProductStock } from "../catalog/ProductStock";
import { Property } from "../catalog/Property";
import { Barcode } from "app/_models/catalog/Barcode";

export class Product {

    constructor(
        public id?: string,
        public name?: string,
        public fullName?: string,
        public description?: string,

        public metaTitle?: string,
        public metaDescription? : string,
        public metaKeywords?: string,

        public code?: string,
        public barcods?: Barcode[],

        public image?: File,
        public prices?: ProductPrice[],
        public stock?: ProductStock[],
        public files?: File[],
        public properties?: Property[],
    ) {

    }
}