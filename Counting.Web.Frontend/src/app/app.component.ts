import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class AppComponent implements OnInit, OnDestroy {
  

  constructor(){

  }

  ngOnInit(): void {
  //  throw new Error("Method not implemented.");
  }

  ngOnDestroy(): void {
    console.info("app-component destroyed");
  }
 
}
