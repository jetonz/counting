import { Routes } from "@angular/router";
import { WarehouseComponent } from './company/warehouse/warehouse.component';
import { DashboardComponent } from "app/dashboard/dashboard.component";
import { WarehouseDetailComponent } from "app/company/warehouse/warehouse-detail/warehouse-detail.component";
import { CounterpartyComponent } from "app/company/counterparty/counterparty.component";
import { ProductComponent } from "app/product/product.component";
import { CashboxComponent } from "app/company/cashbox/cashbox.component";
import { CashboxDetailComponent } from "app/company/cashbox/cashbox-detail/cashbox-detail.component";
import { CounterpartyDetailComponent } from "app/company/counterparty/counterparty-detail/counterparty-detail.component";
import { ProductDetailComponent } from "app/product/product-detail/product-detail.component";


export const appRoutes: Routes = [
  { path: '', component: DashboardComponent },

  { path: 'warehous', component: WarehouseComponent },
  { path: 'warehous/create', component: WarehouseDetailComponent },
  { path: 'warehous/edit/:id', component: WarehouseDetailComponent },

  { path: 'counterparty', component: CounterpartyComponent },
  { path: 'counterparty/create', component: CounterpartyDetailComponent },
  { path: 'counterparty/edit/:id', component: CounterpartyDetailComponent },

  { path: 'products', component: ProductComponent },
  { path: 'products/create', component: ProductDetailComponent },
  { path: 'products/edit/:id', component: ProductDetailComponent },

  { path: 'cashbox', component: CashboxComponent },
  { path: 'cashbox/create', component: CashboxDetailComponent },
  { path: 'cashbox/edit/:id', component: CashboxDetailComponent },

  { path: '**', redirectTo: '' }
]

