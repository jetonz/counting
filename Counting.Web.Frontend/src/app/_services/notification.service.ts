import { Observable } from "rxjs/Observable";
import { Observer } from "rxjs/Rx";
import { Subject } from "rxjs/Subject";
import { Injectable } from "@angular/core";

@Injectable()
//Class is using for subscription and providing different type of messages
export class NotificationService {

    onErrorMessage = new Subject<string>();   
    onSuccessMessage = new Subject<string>();
    onInfoMessage = new Subject<string>();
    

    addError(error: string){
      this.onErrorMessage.next(error)
    }

    addSuccess(success:string){
      this.onSuccessMessage.next(success);
    }


    addInfo(info:string){
      this.onInfoMessage.next(info);
    }

    /*Helper function. Ready made templates*/
    addSuccessUpdate(){
      this.onSuccessMessage.next("Данные добавлены");
    }

    addEditUpdate(){
      this.onSuccessMessage.next("Данные обновлены");
    }

    addDeleteUpdate(){
      this.onSuccessMessage.next("Запись удалена");
    }




    addFailedToUpdate(){
      this.onErrorMessage.next("Не удалось обновить данные");
    }

    addUnableToLoad(){
      this.onErrorMessage.next("Не удалось загрузить данные");
    }

}

 