import { Component, OnInit } from '@angular/core';
import { NotificationService } from "app/_services/notification.service";

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
 
})
export class NotificationComponent implements OnInit {

  message: string = ''; 
  messageType: string = '';
  delayBeforeDisappear = 3000;
  
  constructor(private notificationServ: NotificationService){

  }

  ngOnInit(): void {
    this.handleMessages();
  }

  ngOnDestroy(): void {
    this.notificationServ.onErrorMessage.unsubscribe();
    this.notificationServ.onSuccessMessage.unsubscribe();
    this.notificationServ.onInfoMessage.unsubscribe();

    console.info("app-component destroyed");

  }

  private handleMessages(){

    this.notificationServ.onErrorMessage.subscribe(
      (error: string) =>{
          this.message = error;
          this.messageType = 'alert-danger';
          this.runClose();          
      }
    );

    this.notificationServ.onSuccessMessage.subscribe(
       (success: string) =>{
          this.message = success;
          this.messageType = 'alert-success';
          this.runClose();
      }
    );

    this.notificationServ.onInfoMessage.subscribe(
       (info: string) =>{
          this.message = info;
          this.messageType = 'alert-info';
          this.runClose();
      }
    );


  }

  private runClose(){
      setTimeout(
            ()=>{ this.closeWindow()}
            ,this.delayBeforeDisappear
            ); 
  }

  closeWindow(){
    this.message = '';
  }

}
