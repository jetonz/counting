import { Directive, TemplateRef, ViewContainerRef, Input, Renderer, HostBinding } from '@angular/core';

@Directive({
  selector: '[appBalanceColored]'
})

export class BalanceColorDirective {

  private hasView = false;

  constructor(private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private renderer: Renderer,
  ) {

  }
  

  private assignTo: string;
  //set a property of object to watch for
  @Input() set appBalanceColoredWatchValue(propertyToWatch: string) {
    this.assignTo = propertyToWatch;
  }

  private assignedObject: any;
  //set object to watch for
  @Input() set appBalanceColored(objectToWatch: any) {

    this.assignedObject = objectToWatch;

    if (objectToWatch && !this.hasView) {

      let valueFromObject = this.assignedObject[this.assignTo]; //get value from assigned object by passing property
      
       this.viewContainer.createEmbeddedView(this.templateRef);
     //--- HERE I NEED TO APPLY STYLE AND SET INNER_TEXT ---

      this.hasView = true;
    }
    else if(objectToWatch && this.hasView){
      //update value
    }
    else if (!objectToWatch && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }


}

