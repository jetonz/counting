import { Directive, Input, HostBinding, ElementRef, OnInit, Renderer2 } from '@angular/core';

@Directive({
    selector: '[appTabs]'
})

/**
 * Tabs with specific implementation to look at local storage and keep tabs
 * open on selected tab even view has been reloaded
 * EXAMPLE
 * 
 *  <div appTabs data-storageKey="productsList">
            <ul class="tabs-nav">
                <li data-key="CONTENT_1" data-tab="tab-1"><span href="">Tab 1</span></li>
                <li data-key="CONTENT_2" data-tab="tab-2"><span href="">Tab 2</span></li>
            </ul>
             <div class="tabs-content">
                 <div id="tab-1">
                       CONTENT 1
                 </div>
                 <div id="tab-2">
                       CONTENT 2
                 </div>
             </div>      
                   
                
        </div>
 */
export class TabsDirective implements OnInit {


    private currentTabIndex: number;
    private listArr;
    private storageKey: string;

    constructor(private elementRef: ElementRef,
        private renderer: Renderer2) {

    }


    ngOnInit(): void {
        var nativeEl = this.elementRef.nativeElement as HTMLElement;
        this.listArr = nativeEl.querySelectorAll(".tabs-nav>li");
        const activeTab = this.getTabIdFromStorage();

        const contentArr = nativeEl.querySelectorAll(".tabs-content>div");
        for (let i = 0; i < contentArr.length; i++) {
            const contentDiv = contentArr[i] as HTMLElement;
            contentDiv.style.display = "none";
        }


        let elementFound = false;
        for (let li = 0; li < this.listArr.length; li++) {

            const element = this.listArr[li] as HTMLElement;
            const key = element.dataset.key;
            if (!elementFound && key == activeTab) {
                element.className = element.className + " active";
                this.showContent(element);
                elementFound = true;
                this.currentTabIndex = li;
            }

            this.renderer.listen(element, 'click', (e) => {
                this.changeTab(li);
            })
        }

        //set default first tab as active in case when we visit this page at first        
        if (!elementFound) {
            const firstTab = this.listArr[0];
            this.listArr[0].className = firstTab.className + " active";
            this.currentTabIndex = 0;

            this.showContent(firstTab);
        }

    }



    private changeTab(index: number) {

        if(index == this.currentTabIndex)
            return;

        const oldLiElement = this.listArr[this.currentTabIndex];
        const newLiElement = this.listArr[index];
        const currTabClass = oldLiElement.className as string;

        oldLiElement.className = currTabClass.replace("active", "");
        newLiElement.className = newLiElement.className + " active";

        this.hideContent(oldLiElement);
        this.showContent(newLiElement);

        this.currentTabIndex = index;

        const newTabKey = this.listArr[index].dataset.key;
        if (newTabKey != null)
            this.putToStorage(newTabKey);
    }


    private getTabIdFromStorage(): string {

        this.storageKey = this.elementRef.nativeElement.dataset.storagekey;

        if (this.storageKey == null) return null;

        const savedTabId = localStorage.getItem(this.storageKey);
        return savedTabId;
    }

    private putToStorage(value: string) {
        if (this.storageKey == null) return;

        localStorage.setItem(this.storageKey, value);
    }

    private hideContent(element: HTMLElement) {
        const currentContentDiv = document.getElementById(element.dataset.tab);
        currentContentDiv.style.display = "none";
    }

    private showContent(element: HTMLElement) {
        const currentContentDiv = document.getElementById(element.dataset.tab);
        currentContentDiv.style.display = "block";
    }


}
