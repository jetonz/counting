import { Component, OnInit, Output, EventEmitter, Input, } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Output() itemChanged = new EventEmitter<string>();
  @Input()isVisible : boolean = false;

 
  

  constructor() { }

  ngOnInit() {
  }
 
}
