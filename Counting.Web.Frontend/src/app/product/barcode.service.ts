import { Injectable } from "@angular/core";
import { Barcode } from "app/_models/catalog/Barcode";

@Injectable()
export class BarcodeService {

    private barcodes = [
        new Barcode("1", 6564645454, "1"),
        new Barcode("2", 35728523, "1"),
        new Barcode("3", 1325485789, "2"),
        new Barcode("4", 4879979797, "2"),
        new Barcode("5", 6564645454, "3"),
        new Barcode("6", 35728523, "4"),
        new Barcode("7", 1325485789, "4"),
        new Barcode("8", 4879979797, "4")
    ]



    public getBarcodes(productId: string): Promise<Barcode[]> {
        return new Promise((resolve, reject) => {
            try {

                let filteredList: Barcode[] = [];
                this.barcodes.forEach((element, index) => {

                    if (element.productId == productId) {
                        filteredList.push(element);
                    }

                    if (index + 1 == this.barcodes.length) {
                        resolve(filteredList);
                    }
                });

            }
            catch (ex) {
                reject();
            }
        });
    }

     public addBarcode(productId: string): Promise<boolean> {
        return new Promise((resolve, reject) => {

            try {
                let newId = '' + (this.barcodes.length + 1);               
                let newBarcode = new Barcode(newId, 999999999999, productId);
                         
                this.barcodes.push(newBarcode);
                resolve(true);
            }
            catch (Exc) {
                reject(false);
            }
        }

        );
    }

     public deleteBarcode(id: string) {
        return new Promise((resolve, reject) => {
            let success = false;
            this.barcodes.forEach((element, index) => {

                if (element.id == id) {
                    this.barcodes.splice(index, 1);
                    success = true;
                    return;
                }
            });

            (success) ? resolve(true) : reject(false);

        });
    }


}