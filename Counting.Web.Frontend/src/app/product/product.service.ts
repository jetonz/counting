import { Injectable } from "@angular/core";

import { Product } from "app/_models/catalog/Product";
import { ProductPrice } from "app/_models/catalog/ProductPrice";
import { PriceType } from "app/_models/catalog/PriceType";
import { Warehouse } from "app/_models/catalog/Warehouse";
import { ProductStock } from "app/_models/catalog/ProductStock";
import {File } from "app/_models/catalog/File";
import { Barcode } from "app/_models/catalog/Barcode";

@Injectable()
export class ProductService {

    private mockSellPrice = new PriceType("1","Розничная");
    private mockSelfPrice = new PriceType("2","Закупочная");
    //private mockBarcodes = [ new Barcode("1", 6564645454, "1"), new Barcode("2", 35728523, "1"), new Barcode("3", 1325485789, "1"),new Barcode("4", 4879979797, "1")]


    private mockMainWarehouse = new Warehouse("1", "Основной склад");
    private mockSecondWarehouse = new Warehouse("2", "Дополнительный склад");

    private productList : Product[] = [
        new Product("1", "Разъем Lenovo S820", "Разъем Lenovo S820", "Нихуя себе мега описание бла-бла-бла", "SEo title", "Seo header", "Key descr", "25000", null , null, 
            [new ProductPrice("1", this .mockSellPrice ,50), new ProductPrice("2", this.mockSelfPrice, 10)],
            [new ProductStock("1", this.mockMainWarehouse, 5), new ProductStock("1", this.mockSecondWarehouse, 2)],
            null ,null ),

        new Product("2", "Камера Fly 434 (основная)", "Камера Fly 434 (основная)", "Нихуя себе мега описание бла-бла-бла",  "SEo title", "Seo descr", "Key words","25000",  null , null, 
            [new ProductPrice("1", this .mockSellPrice ,150), new ProductPrice("2", this.mockSelfPrice, 110)],
            [new ProductStock("1", this.mockMainWarehouse, 35), new ProductStock("1", this.mockSecondWarehouse, 22)] ,
            null,null ),

            new Product("3", "Камера Xiaomi Redmi Note 2 (OMIBA08)", "Камера Xiaomi Redmi Note 2 (OMIBA08)", "Нихуя себе мега описание бла-бла-бла",  "SEo title", "Seo descr", "Key words", "25000", null, null, 
            [new ProductPrice("3", this .mockSellPrice , 1050), new ProductPrice("2", this.mockSelfPrice, 100.32)],
            [new ProductStock("3", this.mockMainWarehouse, 1), new ProductStock("1", this.mockSecondWarehouse, 0)],
            null,null ),

            new Product("4", "Шлейф Asus ZenPad Z170MG соеденяющий дисплей и плату", "Шлейф Asus ZenPad Z170MG соеденяющий дисплей и плату", "Нихуя себе мега описание бла-бла-бла", "SEo title", "Seo descr", "Key words", "25000", null, null, 
            [new ProductPrice("1", this .mockSellPrice , 0), new ProductPrice("2", this.mockSelfPrice, 0)],
            [new ProductStock("1", this.mockMainWarehouse, 15), new ProductStock("1", this.mockSecondWarehouse, 12)],
            null,null ),
    ]

    constructor() {

    }


    public getProducts(): Promise<Product[]> {
        return new Promise((resolve, reject) => {
            try {
                resolve(this.productList.slice());
            }
            catch (ex) {
                reject();
            }
        });
    }


    public getProduct(id: string): Promise<Product[]> {
        return new Promise((resolve, reject) => {

            this.productList.forEach(element => {
                if (element.id === id)
                    resolve(element);
            });

            reject();
        });
    }

    public addProduct(newProduct: Product): Promise<boolean> {
        return new Promise((resolve, reject) => {

            try {
                newProduct.id = '' + (this.productList.length + 1);                
                this.productList.push(newProduct);
                resolve(true);
            }
            catch (Exc) {
                reject(false);
            }
        }

        );
    }

    public updateProduct(product: Product): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let success = false;
            this.productList.forEach((element, index) => {

                if (element.id == product.id) {                   
                    element.description = product.description;
                    element.fullName = product.fullName;
                    element.name = product.name;
                    element.barcods = product.barcods;                   
                    element.properties = product.properties;                 
                    success = true;
                    return;

                }
            });

            (success) ? resolve(true) : reject(false);

        });
    }

    public deleteProduct(id: string) {
        return new Promise((resolve, reject) => {
            let success = false;
            this.productList.forEach((element, index) => {

                if (element.id == id) {
                    this.productList.splice(index, 1);
                    success = true;
                    return;
                }
            });

            (success) ? resolve(true) : reject(false);

        });
    }

     public filterCounterparty(text: string): Promise<Product[]> {
        return new Promise((resolve, reject) => {
            if(text == ''){
                resolve(this.productList.slice());
                return;
            }
                
            let filteredList: Product[] = [];
            this.productList.forEach((element, index) => {
 
                if (element.name.startsWith(text)) {
                    filteredList.push(element);
                }

                if (index+1 == this.productList.length) {
                   
                    resolve(filteredList);
                }

            });
        });
    }
}