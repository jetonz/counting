import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { NgForm } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";

import { ProductService } from "app/product/product.service";
import { BarcodeService } from "app/product/barcode.service";

import { Product } from "app/_models/catalog/Product";
import { Property } from "app/_models/catalog/Property";
import { Barcode } from "app/_models/catalog/Barcode";

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  isEdit = false;
  currProduct: Product = new Product();
  selectedProperty: Property;
  selectedBarcode: Barcode;  
  imagesArr : any[] = ["https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-JRS0HQYmkScuDy3IZEf0Ebju9gJCO9EeTm6z6GTXTQBsXDdZ08kiAQ"];

  @ViewChild('uploadButton') uploadBtn : ElementRef;

  constructor(private productService: ProductService,
              private barcodeService: BarcodeService,
              private routerSnapshot: ActivatedRoute,
              private renderer: Renderer2) { }

  ngOnInit() {
    const productId = this.routerSnapshot.snapshot.params['id'];
    if(productId != null){
      this.productService.getProduct(productId)
      .then((data)=>{
        this.currProduct = data;       
      })
       this.isEdit = true;   
    }
     

    this.renderer.listen(this.uploadBtn.nativeElement, "change",
      (data) => { this.uploadFileToView(data); }
    );

    
  }

  loadBarcodes(){ 

    this.barcodeService.getBarcodes(this.currProduct.id)
      .then(data=>{
        this.currProduct.barcods = data;
      })

  }

  deleteBarcode(){
    this.barcodeService.deleteBarcode(this.selectedBarcode.id)
      .then(()=>{
        this.selectedBarcode = null;
        this.barcodeService.getBarcodes(this.currProduct.id)
        .then(data=>{
          this.currProduct.barcods = data;          
        })
      })
  }

  addBarcode(){
    this.barcodeService.addBarcode(this.currProduct.id)
      .then(()=>{
        this.barcodeService.getBarcodes(this.currProduct.id)
          .then(data=>{
            this.currProduct.barcods = data;
          })
      })
  }

  loadProperties(){

  }

  onSaveOrCreate(form: NgForm) {
    if (!form.valid) {
      Object.keys(form.controls).forEach(key => {
        
        form.control.get(key).markAsTouched();
      });
      return;
    }
  }

  openPickFileWindow(){
    this.uploadBtn.nativeElement.click();
  }

  uploadFileToView(data: any){
    console.log(data);
  }

  selectBarcode(barcode: Barcode){
    this.selectedBarcode = barcode;
  }

  selectProperty(property: Property){
    this.selectedProperty = property;
  }
}
