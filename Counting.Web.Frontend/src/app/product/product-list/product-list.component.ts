import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from "@angular/router";

import { ProductService } from "app/product/product.service";
import { Product } from "app/_models/catalog/Product";
import { NotificationService } from "app/_services/notification.service";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  @Output() selectedProduct = new EventEmitter<Product>();

  productList: Product[] = [];
  selectedProductItem: Product;

  constructor(private productServ: ProductService,
    private router: Router,
    private notificationServ: NotificationService) { }

  ngOnInit() {
    this.tryLoadProducts();

  }

  tryLoadProducts() {
    this.productServ.getProducts().then(list => {
      this.productList = list;
      this.onItemSelected(null);
    }).catch(() => {
      this.notificationServ.addUnableToLoad()
    });
  }

  onItemSelected(choosenProduct: Product) {
    this.selectedProductItem = choosenProduct;
    this.selectedProduct.emit(this.selectedProductItem);
  }


  goToDetail(product: Product) {
    this.router.navigate(["products", "edit", product.id]);
  }

  deleteProduct() {

    this.productServ.deleteProduct(this.selectedProductItem.id)
      .then(() => {
        this.notificationServ.addDeleteUpdate();
        this.tryLoadProducts();
      })
      .catch(() => { this.notificationServ.addFailedToUpdate() });

  }

  filterProducts(text: string){
    this.productServ.filterCounterparty(text)
    .then((data)=>{
      this.productList = data;
    });
  }




}
