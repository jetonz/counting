import { Component, OnInit, ViewChild } from '@angular/core';
import { Product } from "app/_models/catalog/Product";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  selectedProduct: Product;
  searchWord: string = '';

  private textFilterTimeoutFn: any;
  private textFilterExecuting: boolean;

  @ViewChild('listComponent') child;

  constructor() { }

  ngOnInit() {
   // this.selectedProduct = 
  }


  onFilterTextEdit() {

    if (this.textFilterExecuting) {
      clearTimeout(this.textFilterTimeoutFn);
    }

    this.textFilterExecuting = true;
    this.textFilterTimeoutFn = setTimeout(() => {
      this.child.filterProducts(this.searchWord);
      this.textFilterExecuting = false;
    }, 700);
  }

  clearSearch() {
    if (this.searchWord != '') {
      this.searchWord = '';
      this.onFilterTextEdit();      
    }

  }

}
