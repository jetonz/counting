import { CountingWebPage } from './app.po';

describe('counting-web App', function() {
  let page: CountingWebPage;

  beforeEach(() => {
    page = new CountingWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
