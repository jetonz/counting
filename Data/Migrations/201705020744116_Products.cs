namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Products : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ParentCatrgory = c.Guid(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        FullName = c.String(),
                        Description = c.String(),
                        CategoryId = c.Guid(nullable: false),
                        MainImage_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.FileContents", t => t.MainImage_Id)
                .Index(t => t.CategoryId)
                .Index(t => t.MainImage_Id);
            
            CreateTable(
                "dbo.FileContents",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        BinaryData = c.Binary(),
                        Product_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .Index(t => t.Product_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "MainImage_Id", "dbo.FileContents");
            DropForeignKey("dbo.Products", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.FileContents", "Product_Id", "dbo.Products");
            DropIndex("dbo.FileContents", new[] { "Product_Id" });
            DropIndex("dbo.Products", new[] { "MainImage_Id" });
            DropIndex("dbo.Products", new[] { "CategoryId" });
            DropTable("dbo.FileContents");
            DropTable("dbo.Products");
            DropTable("dbo.Categories");
        }
    }
}
