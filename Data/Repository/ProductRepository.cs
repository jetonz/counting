﻿using Core.Domain.Global;
using Core.Interfaces.Repository;
using Data.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        public ProductRepository(DataContext context) : base(context)
        {
        }

        public IEnumerable<Product> GetRootProducts()
        {
            return base.Context.Products.Where(x => x.Category == null).ToList();
        }
    }
}
