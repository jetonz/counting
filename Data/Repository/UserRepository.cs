﻿using Core.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain.Global;
using System.Security.Cryptography;

namespace Data.Repository
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(DataContext context) : base(context)
        {
        }

        public string HashPassword(string password)
        {            
            // byte array representation of that string
            byte[] encodedPassword = new UTF8Encoding().GetBytes(password);

            // need MD5 to calculate the hash
            byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);

            // string representation (similar to UNIX format)
            return  BitConverter.ToString(hash)
               // without dashes
               .Replace("-", string.Empty)
               // make lowercase
               .ToLower();
        }

        /// <summary>
        /// Get users using login and hashed password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="hashedPassword">Hashed password</param>
        /// <returns></returns>
        public User Login(string username, string hashedPassword)
        {
            return base.SingleOrDefault(x => x.Login == username && x.Password == hashedPassword);
        }


    }
}
