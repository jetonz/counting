﻿using Core.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;

namespace Data.Repository
{
    public class BaseRepository<Entity> : IRepository<Entity> where Entity : class
    {
        protected DataContext Context { get; private set; }

        public BaseRepository(DataContext context)
        {
            Context = context;
        }

        public void Add(Entity entity)
        {
            Context.Set<Entity>().Add(entity);
        }

        public void AddRange(IEnumerable<Entity> entities)
        {
            Context.Set<Entity>().AddRange(entities);
        }

        public IEnumerable<Entity> Find(Expression<Func<Entity, bool>> predicate)
        {
            return Context.Set<Entity>().Where(predicate).ToList();
        }

        public IEnumerable<Entity> GetAll()
        {
            return Context.Set<Entity>().ToList();
        }

        public Entity Get(object id)
        {
            return Context.Set<Entity>().FirstOrDefault();
        }

        public void Remove(Entity entity)
        {
            Context.Set<Entity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<Entity> entities)
        {
            Context.Set<Entity>().RemoveRange(entities);
        }

        public Entity SingleOrDefault(Expression<Func<Entity, bool>> predicate)
        {
            return Context.Set<Entity>().SingleOrDefault(predicate);
        }
    }
}
