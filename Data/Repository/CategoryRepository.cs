﻿using Core.Domain.Global;
using Core.Interfaces.Repository;
using Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class CategoryRepository : BaseRepository<Category>, ICategoruRepository
    {
        public CategoryRepository(DataContext context) : base(context)
        {
        }

        public IEnumerable<Category> GetRootCategories()
        {
            return base.Context.Categories.Where(x => x.ParentCategory == null).ToList();
        }
    }
}
