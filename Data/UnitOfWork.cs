﻿using Core.Interfaces.Repository;
using Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;

        public UnitOfWork(DataContext context)
        {
            _context = context;
        }

        #region fields
        private IUserRepository _userRepos;
        #endregion




        public IUserRepository UserRepository
        {
            get{
                if (_userRepos == null)
                    _userRepos = new UserRepository(_context);

                return _userRepos;
            }
        }


    }
}
