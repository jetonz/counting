﻿using Autofac;
using Core.Interfaces.Repository;
using Data;
using Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resolver
{
    public static class Register
    {
        
        public static IContainer Container;

        public static void RegisterServices(this ContainerBuilder builder)
        {
            builder.RegisterType<DataContext>().AsSelf();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            Container = builder.Build();

        }

        public static void RegisterServices(this ContainerBuilder builder)
        {
            builder.RegisterType<DataContext>().AsSelf();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            Container = builder.Build();

        }


    }
}
