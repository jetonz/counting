﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Counting.ApplicationUnits
{
    interface ISecurePassword
    {
        System.Security.SecureString Password { get; }
    }
}
