﻿using Core.Domain.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Counting.ApplicationUnits
{
    public class ApplicationUser
    {
        public static Guid Id { get; private set; }

        private ApplicationUser()
        {

        }

        public static void Init(User user)
        {
            if (Guid.Empty != Id)
                throw new InvalidOperationException("User already initialized");

            Id = user.Id;
        }

        public static ApplicationUser Get()
        {
            return (Guid.Empty == Id) ? null : new ApplicationUser();
        }
    }
}
