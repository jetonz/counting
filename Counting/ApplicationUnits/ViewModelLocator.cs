﻿using Counting.ApplicationUnits;
using Counting.ViewModels;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Counting
{
    public static class ViewModelLocator
    {
        public static LoginViewModel LoginViewModel => IocKernel.Get<LoginViewModel>();

    }

}
