﻿using Core.Interfaces.Repository;
using Counting.ApplicationUnits;
using Counting.ViewModels;
using Data;
using Data.Repository;
using Ninject.Modules;
using System.Configuration;
using System.Windows;

namespace Counting
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
       
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            IocKernel.Initialize(new IocConfiguration());
        }


    }

    class IocConfiguration : NinjectModule
    {
        public override void Load()
        {          

            Bind<DataContext>().ToSelf().InTransientScope().WithConstructorArgument("connectionString", ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
            Bind<IUnitOfWork>().To<UnitOfWork>().InTransientScope();
            Bind<IUserRepository>().To<UserRepository>().InTransientScope();
                        

            Bind<LoginViewModel>().ToSelf().InTransientScope(); // Create new instance every time

        }
    }
}
