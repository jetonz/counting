﻿using Autofac;
using Counting.ApplicationUnits;
using Counting.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Counting.Views.Windows
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class LoginWindow : Window, ISecurePassword
    {
        public LoginWindow()
        {
            InitializeComponent();
            var vm = ViewModelLocator.LoginViewModel;
            DataContext = vm;

            vm.CloseAndOpenMain += () =>
            {
                LoginWindow main = Application.Current.MainWindow as LoginWindow;
                if (main != null)
                {
                    MainWindow newWindow = new MainWindow();
                    newWindow.Show();
                    main.Close();
                }
            };
        }

        public SecureString Password => this.PasswordBox.SecurePassword;
        
    }
}
