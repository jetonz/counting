﻿using Core.Domain.Global;
using Core.Interfaces.Repository;
using Counting.ApplicationUnits;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Counting.ViewModels
{
    public delegate void CloseEvent();

    public class LoginViewModel : BaseViewModel
    {
        private readonly IUserRepository _userRepository;

        #region View Properties

        private string _Login;
        public string Login
        {
            get
            {
                return _Login;
            }
            set
            {
                _Login = value.ToLower();
              
            }
        }


        private string _error;
        public string Error
        {
            get
            {
                return _error;
            }
            set
            {
                _error = value;
                OnPropertyChanged("Error");
            }
        }

        public ObservableCollection<string> _UsersList;
        public ObservableCollection<string> UsersList
        {
            get
            {
                return _UsersList;
            }
            set
            {
                _UsersList = value;
                OnPropertyChanged("MastersFilterList");
            }
        }

        public ICommand TryLogin { get; private set; }

        public event CloseEvent CloseAndOpenMain;
        #endregion


        public LoginViewModel(IUserRepository userRepo)
        {
            _userRepository = userRepo;
            TryLogin = new RelayCommand(DoLogin);

            _UsersList = new ObservableCollection<string>();

            var list = _userRepository.GetAll();
            foreach (var user in list)
            {
                _UsersList.Add(user.Login);
            }

        }


        public void DoLogin(object parameter)
        {           

            var passwordContainer = parameter as ISecurePassword;
            string passwordValue = ConvertToUnsecureString(passwordContainer.Password);

            if (string.IsNullOrEmpty(Login)){
                Error = "Выберите пользователя";
                return;
            }

            if (string.IsNullOrEmpty(passwordValue))
            {
                Error = "Введите пароль";
                return;
            }

            string hashedPassword = _userRepository.HashPassword(passwordValue);

            User dbUser = _userRepository.Login(Login, hashedPassword);

            if (dbUser == null)
            {
                Error = "Неверный пароль";
                return;
            }
            else
            {
                ApplicationUser.Init(dbUser);
                DoCloseView();
            }

        }


        protected void DoCloseView()
        {
            var handler = CloseAndOpenMain;
            if (handler != null)
                CloseAndOpenMain();
        }

        /// <summary>
        /// Decript password string
        /// </summary>
        /// <param name="securePassword"></param>
        /// <returns></returns>
        private string ConvertToUnsecureString(System.Security.SecureString securePassword)
        {
            if (securePassword == null)
            {
                return string.Empty;
            }

            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = System.Runtime.InteropServices.Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return System.Runtime.InteropServices.Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }
    }
}
