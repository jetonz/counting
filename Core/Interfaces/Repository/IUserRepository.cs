﻿using Core.Domain.Global;
using Core.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces.Repository
{
    public interface IUserRepository : IRepository<User>
    {
        User Login(string username, string hashedPassword);
        string HashPassword(string password);
    }
}
