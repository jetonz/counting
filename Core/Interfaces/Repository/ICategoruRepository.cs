﻿using Core.Domain.Global;
using Core.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces.Repository
{
    public interface ICategoruRepository : IRepository<Category>
    {
        IEnumerable<Category> GetRootCategories();
    }
}
