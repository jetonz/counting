﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Global
{
    public class Product
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string FullName { get; set; }

        public string Description { get; set; }

        public FileContent MainImage { get; set; }

        public Guid CategoryId { get; set; }

        public virtual Category Category { get; set; }
        
        public virtual ICollection<FileContent> AttachedFiles { get; set; }
    }
}
