﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Global
{
    public class FileContent
    {
        public Guid Id { get; set; }

        public byte[] BinaryData { get; set; }
    }
}
